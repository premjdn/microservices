package hello.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import hello.repository.StoreSupply_repository;
import hello.repository.Store_repository;
import hello.service.StoreService;
import hello.service.StoreServiceimpl;
import hello.service.Store_supplyService;
import hello.service.Store_supplyServiceImpl;

@Configuration
public class RootConfiguration {

    @Bean
    public StoreService storeService(Store_repository storeRepository) {
        return new StoreServiceimpl(storeRepository);
    }
    @Bean
    public Store_supplyService storesupllyService(StoreSupply_repository storesupplyRepository) {
        return new Store_supplyServiceImpl(storesupplyRepository);
    }
}
