package hello.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hello.domain.Store;
import hello.service.StoreService;


@RestController
@RequestMapping("/stores")
public class StoreController {
	 private final StoreService storeService;

	    @Autowired
	    public StoreController(StoreService storeService) {
	        this.storeService = storeService;
	    }

	    @GetMapping(path = "/{id}")
	    public Store get(@PathVariable("id") int id) {
	        return this.storeService.findOne(id);
	    }

	    @PostMapping
	    public ResponseEntity<Store> save(@RequestBody Store store) {
	        Store savedStore = this.storeService.save(store);
	        return new ResponseEntity<>(savedStore, HttpStatus.CREATED);
	    }

	    @PutMapping
	    public ResponseEntity<Store> update(@RequestBody Store store) {
	        Store savedStore = this.storeService.update(store);
	        return new ResponseEntity<>(savedStore, HttpStatus.CREATED);
	    }

	    @DeleteMapping(path = "/{id}")
	    public ResponseEntity<String> delete(@PathVariable("id") int id) {
	        this.storeService.delete(id);
	        return new ResponseEntity<>("Deleted", HttpStatus.ACCEPTED);
	    }

	    @GetMapping(path = "/fromstate/{state}")
	    public List<Store> findstoresInState(@PathVariable("state") String state) {
	        return this.storeService.findstoresInState(state);
	    }
}
