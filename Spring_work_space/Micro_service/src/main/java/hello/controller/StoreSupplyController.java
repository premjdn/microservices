package hello.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hello.domain.Store;
import hello.domain.Store_Supply;
import hello.service.StoreService;
import hello.service.Store_supplyService;

@RestController
@RequestMapping("/postage")
public class StoreSupplyController {
	private final Store_supplyService storesupplyService;
	 @Autowired
	    public StoreSupplyController(Store_supplyService storesupplyService) {
	        this.storesupplyService = storesupplyService;
	    }

	    /*@GetMapping(path = "/{sku}")
	    public Store_Supply get(@PathVariable("sku") int sku) {
	        return this.storesupplyService.findOne(sku);
	    }*/
	  //we need put sku for example http://localhost:8080/postage/1234567
	    @GetMapping(path = "/{skuStores}")
	    public List<Store_Supply> getallstores(@PathVariable("skuStores") int skuStores) {
	    	List<Store_Supply> storelist = this.storesupplyService.allSkurecords(skuStores);
	        return storelist;
	    }

	    @PostMapping
	    public ResponseEntity<Store_Supply> save(@RequestBody Store_Supply store_sup) {
	    	Store_Supply savedstoreSup = this.storesupplyService.save(store_sup);
	        return new ResponseEntity<>(savedstoreSup, HttpStatus.CREATED);
	    }

	    @PutMapping
	    public ResponseEntity<Store_Supply> update(@RequestBody Store_Supply store_sup) {
	        Store_Supply savedstore = this.storesupplyService.update(store_sup);
	        return new ResponseEntity<>(savedstore, HttpStatus.CREATED);
	    }

	    @DeleteMapping(path = "/{sku}")
	    public ResponseEntity<String> delete(@PathVariable("sku") int sku) {
	        this.storesupplyService.delete(sku);
	        return new ResponseEntity<>("Deleted", HttpStatus.ACCEPTED);
	    }
}
