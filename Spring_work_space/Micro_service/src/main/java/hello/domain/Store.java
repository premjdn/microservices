package hello.domain;

import java.io.Serializable;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import com.datastax.driver.core.DataType.Name;
@Table("store")
public class Store implements Serializable {
	 private static final long serialVersionUID = 1L;

	    @PrimaryKey
	    private int id;

	    private String name;

	    private String address;

	    private String state;

	    private String zip;
	    @CassandraType(type = Name.UDT, userTypeName = "addrs")
	    private detaill detaill;

	    public detaill getDetail() {
			return detaill;
		}

		public void setDetail(detaill detaill) {
			this.detaill = detaill;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}

		public Store() {
	    }

	    public Store(String name) {
	        this.name = name;
	    }

	    public int getId() {
	        return id;
	    }

	    public String getName() {
	        return this.name;
	    }

	    public String getAddress() {
	        return this.address;
	    }

	    public String getZip() {
	        return this.zip;
	    }

	    public void setId(int id) {
	        this.id = id;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public void setAddress(String address) {
	        this.address = address;
	    }

	    public void setZip(String zip) {
	        this.zip = zip;
	    }

	    public String getState() {
	        return state;
	    }

	    public void setState(String state) {
	        this.state = state;
	    }

}
