package hello.domain;

import java.io.Serializable;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;
@Table("store_supply")
public class Store_Supply implements Serializable {
	 private static final long serialVersionUID = 1L;
	 @PrimaryKey
	    private int sku;
	   private String store;
	    private String inventory;
	    public int getSku() {
		return sku;
	}
	public void setSku(int sku) {
		this.sku = sku;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getInventory() {
		return inventory;
	}
	public void setInventory(String inventory) {
		this.inventory = inventory;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}
