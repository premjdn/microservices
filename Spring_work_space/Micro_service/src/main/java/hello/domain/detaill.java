package hello.domain;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.UserDefinedType;

import com.datastax.driver.core.DataType.Name;

@UserDefinedType("addrs")
public class detaill {
	@CassandraType(type = Name.TEXT) 
	 private String vill;
	@CassandraType(type = Name.TEXT)
	private String city;
	 public String getVill() {
		return vill;
	}
	public void setVill(String vill) {
		this.vill = vill;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	

}
