package hello.repository;

import java.util.List;

import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;

import hello.domain.Store_Supply;

@Repository
public class CassandraStoreSupplyRepository implements StoreSupply_repository{
	private  CassandraOperations cassandraTemplate;
	public CassandraStoreSupplyRepository(CassandraOperations cassandraTemplate) {
        this.cassandraTemplate = cassandraTemplate;
    }
	@Override
	public Store_Supply save(Store_Supply store_Supply) {
		return this.cassandraTemplate.insert(store_Supply);
	}

	@Override
	public Store_Supply update(Store_Supply store_Supply) {
		return this.cassandraTemplate.update(store_Supply);
	}

	@Override
	public Store_Supply findOne(int sku) {
		return this.cassandraTemplate.selectOneById(Store_Supply.class, sku);
	}

	@Override
	public void delete(int sku) {
		this.cassandraTemplate.deleteById(Store_Supply.class, sku);
		
	}
	@Override
	public List<Store_Supply> findByInventory(String inventory) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Store_Supply> allSkurecords(int sku) {
		//String cqlAll = "select * from store_Supply";
		//List<Store_Supply> results ;
		Select select = QueryBuilder.select().from("store_Supply");
		select.where(QueryBuilder.eq("sku", sku));
        List<Store_Supply> result = cassandraTemplate.select(select, Store_Supply.class);
		
		return result;
	}

	/*@Override
	public List<Store_Supply> findByInventory(String inventory) {
		 Select select = QueryBuilder.select().from("stores_by_state");
	        select.where(QueryBuilder.eq("state", state));
	        return this.cassandraTemplate.select(select, Store.class);
	}*/

}
