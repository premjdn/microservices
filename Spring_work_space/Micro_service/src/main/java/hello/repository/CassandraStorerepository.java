package hello.repository;

import java.util.List;

import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;

import hello.domain.Store;

@Repository
public class CassandraStorerepository implements Store_repository {

	private  CassandraOperations cassandraTemplate;
	 public CassandraStorerepository(CassandraOperations cassandraTemplate) {
	        this.cassandraTemplate = cassandraTemplate;
	    }
	@Override
	public Store save(Store store) {
		return this.cassandraTemplate.insert(store);
	}

	@Override
	public Store update(Store store) {
		return this.cassandraTemplate.update(store);
	}

	@Override
	public Store findOne(int storeId) {
		return this.cassandraTemplate.selectOneById(Store.class, storeId);
	}

	@Override
	public void delete(int storeId) {
		this.cassandraTemplate.deleteById(Store.class, storeId);
		
	}

	@Override
	public List<Store> findByState(String state) {
		 Select select = QueryBuilder.select().from("stores_by_state");
	        select.where(QueryBuilder.eq("state", state));
	        return this.cassandraTemplate.select(select, Store.class);
		
	}

}
