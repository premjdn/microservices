package hello.repository;

import java.util.List;

import hello.domain.Store_Supply;

public interface StoreSupply_repository {
	Store_Supply save(Store_Supply store_Supply);
	Store_Supply update(Store_Supply store_Supply);
	Store_Supply findOne(int sku);
	List<Store_Supply> allSkurecords(int sku);
	    void delete(int sku);
	    List<Store_Supply> findByInventory(String inventory);
}
