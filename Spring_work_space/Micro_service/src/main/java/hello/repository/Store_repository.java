package hello.repository;

import java.util.List;
import hello.domain.Store;


public interface Store_repository {
	 Store save(Store store);
	 Store update(Store store);
	 Store findOne(int storeId);
	    void delete(int storeId);
	    List<Store> findByState(String state);
}
