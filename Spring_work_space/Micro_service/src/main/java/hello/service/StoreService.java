package hello.service;

import java.util.List;

import hello.domain.Store;


public interface StoreService {
	 Store save(Store store);
	 Store update(Store store);
	    Store findOne(int id);
	    void delete(int id);
	    List<Store> findstoresInState(String state);
}
