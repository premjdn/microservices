package hello.service;

import java.util.List;
import java.util.UUID;
import hello.domain.Store;
import hello.repository.Store_repository;

public class StoreServiceimpl implements StoreService {

	private  Store_repository storeRepository;
	public StoreServiceimpl(Store_repository storeRepository) {
                this.storeRepository = storeRepository;
              }
	@Override
	public Store save(Store store) {
		 {
		       
		        this.storeRepository.save(store);
		        return store;
		    }
	}

	@Override
	public Store update(Store store) {
		Store existingStore = this.storeRepository.findOne(store.getId());
        if (existingStore != null) {
            this.storeRepository.update(store);
        }
        return store;
	}

	@Override
	public Store findOne(int id) {
		return this.storeRepository.findOne(id);
	}

	@Override
	public void delete(int id) {
		Store store = this.storeRepository.findOne(id);
        if (store != null) {
            this.storeRepository.delete(id);
        }
	}

	@Override
	public List<Store> findstoresInState(String state) {
		return this.storeRepository.findByState(state);
	}
	
}
 