package hello.service;

import java.util.List;

import hello.domain.Store;
import hello.domain.Store_Supply;
import hello.repository.StoreSupply_repository;
import hello.repository.Store_repository;


public class Store_supplyServiceImpl implements Store_supplyService {
    
	private  StoreSupply_repository storeSupplyRepository;
	public Store_supplyServiceImpl(StoreSupply_repository storeSupplyRepository) {
                this.storeSupplyRepository = storeSupplyRepository;
              }
	@Override
	public Store_Supply save(Store_Supply store_Supply) {
		this.storeSupplyRepository.save(store_Supply);
        return store_Supply;
	}

	@Override
	public Store_Supply update(Store_Supply store_Supply) {
		Store_Supply existingStoreSupply = this.storeSupplyRepository.findOne(store_Supply.getSku());
        if (existingStoreSupply != null) {
            this.storeSupplyRepository.update(store_Supply);
        }
        return store_Supply;
	}

	@Override
	public Store_Supply findOne(int sku) {
		return this.storeSupplyRepository.findOne(sku);
	}

	@Override
	public void delete(int sku) {
		Store_Supply storesup = this.storeSupplyRepository.findOne(sku);
        if (storesup != null) {
            this.storeSupplyRepository.delete(sku);
        }
	}

	@Override
	public List<Store_Supply> findByInventory(String inventory) {
		// TODO Auto-generated method stub
		return this.storeSupplyRepository.findByInventory(inventory);
	}
	@Override
	public List<Store_Supply> allSkurecords(int sku) {

		List<Store_Supply> results = this.storeSupplyRepository.allSkurecords(sku);
		return results;
	}

}
