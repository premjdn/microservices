CREATE KEYSPACE IF NOT EXISTS microservice WITH replication = {'class':'SimpleStrategy', 'replication_factor':1};
CREATE TABLE IF NOT EXISTS  microservice.store (
    id int,
    name varchar,
    address varchar,
    state varchar,
    zip varchar,
    primary key((id), name)
);

CREATE TABLE IF NOT EXISTS  microservice.store (
    sku int,
    store varchar,
    inventory varchar,
    primary key((sku), store)
);



*************************************services**************************
for store post:- http://localhost:8080/stores
request body:-
{
    "id": 3,
    "name": "Store3",
    "address": "Mathura",
    "state": "U.P",
    "zip": "22222"
}
get "http://localhost:8080/stores/1"

For store supply post :-http://localhost:8080/postage
request body:-
{
    "sku": 258,
    "store": "Store1",
    "inventory": "1000"
}

get http://localhost:8080/postage/1234567